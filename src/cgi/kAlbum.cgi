#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kAlbum.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	26.02.2003
# version:	v01_20041119
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
use Fcntl;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;

# ------------------
# declarations
# ------------------

my @picture_dir			= &kCom_getIniValue("kAlbum","PATH_ALBUM");
my @mini_thumb_width	= &kCom_getIniValue("kAlbum","MINI_THUMB_WIDTH");
my @mini_thumb_height	= &kCom_getIniValue("kAlbum","MINI_THUMB_HEIGHT");
my $query_string		= $ENV{QUERY_STRING};


# ------------------
# main
# ------------------
&kCom_printTop("kAlbum");
if ($query_string eq ""){
	&kAlbum_albumOverview;
} elsif ($query_string =~ m/^album:(.*)/) {
	&kAlbum_openAlbum($1);
} elsif ($query_string =~ m/^pic:(.*)/) {
	&kAlbum_showPicture($1);
} elsif ($query_string =~ m/^com:(.*)/) {
	&kAlbum_addComment($1);
} elsif ($query_string =~ m/^update:(.*)/) {
	&kAlbum_update($1);
}
&kCom_printBottom;

# ------------------
# sub
# ------------------
sub kAlbum_albumOverview{
	my @files;
	my $file;
	my @album_status;
	my $album_title;
	my @when;
	my $total_pictures;
	my $no_pic_album;
	my @thumbnails;
	my $thumbnail;
	my $tmp = 0;
	my @titlecolor = &kCom_getIniValue("kCom","HTML_TITLECOLOR");
	my @oddbgcolor = &kCom_getIniValue("kCom","HTML_UNEVENCOLOR");

	if (&kCom_login){
		# gets all directories in the album directory
		opendir (D, "$picture_dir[0]");
		@files = grep /\w/, readdir(D);
		closedir(D);

		kCom_tableStart("Albums","","-1","2");
		print "<table width=\"100%\"><tr><td>&nbsp;</td><td><font color=\"#$titlecolor[0]\"><b>Title:</b></font></td><td><font color=\"#$titlecolor[0]\"><b>When:</b></font></td><td><font color=\"#$titlecolor[0]\"><b>No. pics:</b></font></td></tr>\n";

		# sorting reverse to get the newest album on the top
		foreach $file (reverse sort @files){

			@album_status = &kCom_getIniValue("$picture_dir[0]/$file/$file\.txt","STATUS");
			@when = &kCom_getIniValue("$picture_dir[0]/$file/$file\.txt","WHEN");

			# status: 0 - not published | 1 - published
			if ($album_status[0] == 1){
				$album_title = kAlbum_getAlbumTitle($file);

				# counts the number of pictures in the album
				opendir (D, "$picture_dir[0]/$file");
				my @thumbnails = grep /\_thumb\.jpg/, readdir(D);
				closedir (D);

				$no_pic_album = 0;
				foreach (@thumbnails){
					$no_pic_album++;
				}

				$thumbnail = $thumbnails[rand($#thumbnails+1)];

				if ($tmp == 1) {
					$tmp = 0;
					print "<tr bgcolor = \"#$oddbgcolor[0]\">\n";
				} else {
					$tmp++;
					print "<tr>\n";
				}

				print qq(
						<td align="center">
							<a href="kAlbum.cgi?album:$file"><img width="$mini_thumb_width[0]" height="$mini_thumb_height[0]" src=\"$picture_dir[0]/$file/$thumbnail\" title="Open the album '$album_title'" border="0"></a>
						</td>
						<td><a class="noLine" href="kAlbum.cgi?album:$file"><h2>$album_title</h2></a></td>
						<td>$when[0]</td>
						<td align="right">$no_pic_album&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				);
				$total_pictures += $no_pic_album;
			}
		}
		print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\">= $total_pictures&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>\n";
		kCom_tableStop;
	} else {
		&kCom_notLoggedInMessage;
	}

}



sub kAlbum_openAlbum{
	my $album = $_[0];
	my $album_title = &kAlbum_getAlbumTitle($album);
	my $thumbnail;
	my $picture;
	my @subtitle = &kCom_getIniValue("$picture_dir[0]/$album/$album\.txt","SUBTITLE");
	my @when = &kCom_getIniValue("$picture_dir[0]/$album/$album\.txt","WHEN");
	my @where = &kCom_getIniValue("$picture_dir[0]/$album/$album\.txt","WHERE");
	my @pic_desc;

	opendir (D, "$picture_dir[0]/$album");
	my @thumbnails = grep /\_thumb\.jpg/, readdir(D);
	closedir (D);

	&kCom_tableStart("$album_title");
	print "<blockquote><center><h3>$subtitle[0]</h3><i>$where[0]<br>$when[0]</i><br><br>";
	foreach $thumbnail (sort @thumbnails){
		$picture = $thumbnail;
		$picture =~ s/\_thumb\.jpg$//;
		@pic_desc = &kCom_getIniValue("$picture_dir[0]/$album/$picture\.txt","DESCRIPTION","1");
		$pic_desc[0] =~ s/"/'/g;
		print "<a href=\"kAlbum.cgi?pic:$picture\.jpg\"><img src=\"$picture_dir[0]/$album/$thumbnail\" border=\"0\" title=\"$pic_desc[0]\"></a>\n";
	}
	print "</center></blockquote>";
	kCom_tableStop;
}


sub kAlbum_showPicture{
	my $picture				= $_[0];
	my $edit				= 0;
	if ($picture =~ m/(.*)\?update$/){
		$edit		= 1;
		$picture	= $1;
	}
	my $stripped_picture	= $picture;
	$stripped_picture		=~ s/\.jpg$//;
	my ($album,$pic)		= split("-",$picture);
	my $pic_textfile		= $picture;
	$pic_textfile			=~ s/\.jpg$/\.txt/;
	my @pic_desc			= &kCom_getIniValue("$picture_dir[0]/$album/$pic_textfile","DESCRIPTION","1");
	my @display_edit		= &kCom_getIniValue("kCom","DISPLAY_EDIT");
	my $prev_pic			= "album:$album";
	my $next_pic;
	my $thumbnail;
	my $picture2;
	my $pic_no				= 0;
	my $tot_pics			= 0;
	my $i;
	my $album_title;
	my @sorted_thumbs;


	opendir (D, "$picture_dir[0]/$album");
	my @thumbnails = grep /\_thumb\.jpg/, readdir(D);
	closedir (D);

	# Finds the previous and next picture.
	# If there are no previous / next we use
	# album index instead
	foreach $thumbnail (sort @thumbnails){
		push (@sorted_thumbs, $thumbnail);
		$picture2 = $thumbnail;
		$picture2 =~ s/\_thumb\.jpg$/.jpg/;

		$tot_pics++;

		if ($next_pic eq "album:$album"){	# if "this picture" is found, then this is the next pic
			$next_pic = "pic:" . $picture2;
		} elsif ($picture eq $picture2){	# we have found "this picture", then we set "next_pic" to the name of the album.
			$next_pic = "album:$album";		# this is done to signalise that "next_pic" can be set next round
			$pic_no = $tot_pics;			# we store the number ($pic_no) "this pictures" is in the @sorted_thumbs array
		} elsif ($next_pic eq undef) {		# the prev_pic is only set as long as we haven't found this picture
			$prev_pic = "pic:" .$picture2;
		}
	}

	$album_title = &kAlbum_getAlbumTitle($album);

	print qq(
		<center>
		<table border="0">
		<tr><td align="center">[ $album_title | $pic_no/$tot_pics | <a href="kAlbum.cgi?album:$album">index</a> | <a href="kAlbum.cgi?$prev_pic">previous</a> | <a href="kAlbum.cgi?$next_pic">next</a> ]</td></tr>
		<tr>
			<td align="center">
				<img class="border" src="$picture_dir[0]/$album/$picture">
			</td>
		</tr>
		<tr>
			<td align="center">
			);
	if ($edit){
		print qq(
			<form name="kAlbum_caption" action="kAlbum.cgi?update:caption" method="post">
			<input type="hidden" name="picture" value="$stripped_picture\.jpg">
			<input type="hidden" name="album" value="$album">
			<input type="text" name="pic_desc" value="$pic_desc[0]" size="100"></form>
		);
	} else {
		print $pic_desc[0];
	}
	print qq(
			</td>
		</tr>
		<tr>
			);
	if ($edit){
		print qq(<td align="right">[<a href="javascript:document.kAlbum_caption.submit()">update caption</a>]</td>);
	} else {
		print qq(<td align="right">[<a href="kAlbum.cgi?com:$picture">add comment</a>]</td> );
	}
	print qq(
		</tr>
		<tr>
			<td align="center">
	);
	for ($i=($pic_no-4); $i<=($pic_no+4); $i++){
		if ($sorted_thumbs[($i)] ne undef and $i >= 0){
			$picture = $sorted_thumbs[($i)];
			$picture =~ s/\_thumb\.jpg$//;
			@pic_desc = &kCom_getIniValue("$picture_dir[0]/$album/$picture\.txt","DESCRIPTION","1");
			$pic_desc[0] =~ s/"/'/g;
			if ($i == ($pic_no-1) ){																# have to subtract 1 due to we start counting at 1, not 0 as $i does
				print qq(<a href="kAlbum.cgi?pic:$picture\.jpg"><img border="4" width="$mini_thumb_width[0]" height="$mini_thumb_height[0]" src="$picture_dir[0]/$album/$sorted_thumbs[($i)]" title="$pic_desc[0]"></a>&nbsp;\n);
			} else {
				print qq(<a href="kAlbum.cgi?pic:$picture\.jpg"><img border="0" width="$mini_thumb_width[0]" height="$mini_thumb_height[0]" src="$picture_dir[0]/$album/$sorted_thumbs[($i)]" title="$pic_desc[0]"></a>&nbsp;\n);
			}
		}
	}

	print qq(
			</td>
		<tr>
			<td>&nbsp;</td></tr>
		<tr>
			<td>
	);

	if (-e "$picture_dir[0]/$album/$stripped_picture\.com"){
		if ($edit){
			print qq(
				<form name="kAlbum_comments" action="kAlbum.cgi?update:comments" method="post">
				<input type="hidden" name="picture" value="$stripped_picture\.jpg">
				<input type="hidden" name="album" value="$album">
				<textarea name="comments" rows="10" cols="170" wrap="physical">);
			&kCom_listComments("$picture_dir[0]/$album/$stripped_picture\.com",1);
			print qq(</textarea></td></tr>
				<tr><td align="right">[<a href="javascript:document.kAlbum_comments.submit()">update comments</a>]
			);
		} else {
			&kCom_listComments("$picture_dir[0]/$album/$stripped_picture\.com");
		}
	}

	print qq(
			</td>
		</tr>
		</table>
		</center>
	);
}


sub kAlbum_update{
	my $type = $_[0];
	my %FORM = &kCom_parse(1);
	my $pic_desc = $FORM{'pic_desc'};
	my $comments = $FORM{'comments'};
	my $picture = $FORM{'picture'};
	my $stripped_picture	= $picture;
	$stripped_picture		=~ s/\.jpg$//;
	my $album = $FORM{'album'};

	if ($type eq "caption"){
		open (FILE, ">$picture_dir[0]/$album/$stripped_picture\.txt") || &kCom_error("kAlbum_update","failed to open '$picture_dir[0]/$album/$stripped_picture\.txt'");
		flock (FILE, 2);
		print FILE "DESCRIPTION\t".$pic_desc;
		close (FILE);
		flock (FILE, 8);
	} elsif ($type eq "comments"){
		open (FILE, ">$picture_dir[0]/$album/$stripped_picture\.com") || &kCom_error("kAlbum_update","failed to open '$picture_dir[0]/$album/$stripped_picture\.com'");
		flock (FILE, 2);
		print FILE $comments;
		close (FILE);
		flock (FILE, 8);
	}
	
	&kCom_jumpTo("kAlbum.cgi?pic:$picture");
}


sub kAlbum_addComment{
	my $picture = $_[0];
	$picture =~ s/\.jpg$//;
	my ($album,$pic) = split("-",$picture);
	my $return_url = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}?$ENV{'QUERY_STRING'}";
	$return_url =~ s/com:/pic:/;

	kCom_addComment("$picture_dir[0]/$album/$picture\.com","$picture_dir[0]/$album/$picture\_thumb.jpg",$return_url);
}


sub kAlbum_getAlbumTitle{
	my $album = $_[0];
	$album =~ s/^\d+\_//g;
	$album =~ s/\_/ /g;
	return $album;
}


