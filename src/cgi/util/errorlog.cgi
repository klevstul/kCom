#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

##########################################################################################
#
#	errorlog.cgi
#	�2000, Perl Services
#
#	Requirements:		UNIX Server, Perl5+
#	Created:		September 22nd, 2000
#	Author: 		Jim Melanson
#	Contact:		www.perlservices.net
#				Phone:		1-705-437-3283
#				Fax:		1-208-694-1613
#				E-Mail:		info@perlservices.net
#
##########################################################################################
#
#	Log File Path:
#	Set  this variable to the absolute path to your servers/domains error log. This is
#	the error log written to by the Perl interpretor on STDERR. You will usually have to
#	ask your support/host/SysAdmin for this path.

#$ERR_LOG = "/home/klevstul/log/error_log";
$ERR_LOG = "C:/klevstul/private/projects/kCom/logs/error_log";

#
##########################################################################################
#
#					EDIT NOTHING
#				      BELOW THIS LINE
#
##########################################################################################

$ScriptURL = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}";
$FONT = "<FONT NAME=\"verdana,helvetica,arial\" SIZE=\"2\">";
$FONT3 = "<FONT NAME=\"verdana,helvetica,arial\" SIZE=\"3\">";


&Parse;

$FORM{numlines} ||= 25;
$FORM{reverse} ||= 1;
$FORM{striphtml} ||= 1;

print "Content-type: text/html\n\n";
print "<HEAD><TITLE>Error Log Viewer</TITLE></HEAD><BODY BGCOLOR=\"#F9F9F9\" TEXT=\"NAVY\" LINK=\"ROYALBLUE\" 
ALINK=\"ROYALBLUE\" VLINK=\"ROYALBLUE\">\n";
print "<TABLE BORDER=0><TR><TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$FONT\n";
print "</TD><TD><FONT FACE=\"verdana,helvetica,arial\" SIZE=7><B>Error Log Viewer</B></FONT><BR>$FONT Provided by <A 
HREF=\"http://www.perlservices.net/\">Perl Services</A></FONT></TD></TR>\n";
print "</TABLE><BR>\n";
print "<TABLE BORDER=2 WIDTH=100% CELLPADDING=5><TR BGCOLOR=\"#000033\"><TD ALIGN=\"CENTER\"><FONT 
COLOR=\"WHITE\">$FONT<B>Options:</B><BR>\n";
  print "<FORM ACTION=\"$ScriptURL\" METHOD=\"POST\">\n";
  print "Lines:<INPUT TYPE=\"TEXT\" NAME=\"numlines\" VALUE=\"$FORM{numlines}\" SIZE=3>";
  print "&nbsp;" x 5, "Order Returns:<SELECT NAME=\"reverse\" SIZE=1>";
    if($FORM{reverse} == 1) {
	print "<OPTION VALUE=\"1\" SELECTED>Ascending (Newest first)</OPTION><OPTION VALUE=\"2\">Descending (Newest 
last)</OPTION>";
    } else {
	print "<OPTION VALUE=\"1\">Ascending (Newest first)</OPTION><OPTION VALUE=\"2\" SELECTED>Descending (Newest 
last)</OPTION>";
    }
  print "</SELECT>";
  print "&nbsp;" x 5, "Strip HTML:<SELECT NAME=\"striphtml\" SIZE=1>";
    if($FORM{striphtml} == 2) {
	print "<OPTION VALUE=\"2\" SELECTED>No</OPTION><OPTION VALUE=\"1\">Yes</OPTION>";
    } else {
	print "<OPTION VALUE=\"2\">No</OPTION><OPTION VALUE=\"1\" SELECTED>Yes</OPTION>";
    }
  print "</SELECT>";

print "&nbsp;" x 5, "<INPUT TYPE=\"SUBMIT\" VALUE=\"Get\"></FORM></FONT></FONT></TD></TR></TABLE>\n";
print "<BR>$FONT3\n";
if(open(LOG, "<$ERR_LOG")) {
    my @temp = <LOG>;
    close LOG;
    chomp(@temp);
    my @lines; my $start; my $end;
    if($FORM{reverse} == 1) {
	@lines = reverse(@temp);
	$start = 0;
	$end = $FORM{numlines};
    } else {
	@lines = @temp;
	$start = $#lines - $FORM{numlines};
	$end = $#lines;
    }
    undef(@temp);
    if($start < 0) {
	$start = 0;
    }
    for(my $a = $start; $a <= $end; $a++) {
	if($FORM{striphtml} == 1) {
	    $lines[$a] =~ s/<.+>?//g;
	}
	print "$lines[$a]<BR>\n";
	if($lines[$a] =~ /^\[/) {print "<HR>"}
    }
} else {
    print "<BR><BR><BR><FONT COLOR=\"#FF0000\"><B>Unable to open Error Log!</B><BR><BR>Is the path \"$ERR_LOG\" 
correct?<BR><BR><BR><BR>\n";
}
print "</FONT><BR><BR>\n";
print "<TABLE BORDER=2 WIDTH=100% CELLPADDING=5><TR BGCOLOR=\"#000033\"><TD ALIGN=\"CENTER\">$FONT<FONT COLOR=\"#FFFFFF\">\n";
print "&copy; 2000-2001 <A HREF=\"http://www.perlservices.net/\"><FONT COLOR=\"#FFFFFF\">Perl Services</FONT></A>\n";
print "</FONT></FONT></TD></TR></TABLE>\n";
print "<BR><BR></BODY></HTML>\n";

sub Parse {
    local($name, $value, $buffer, $pair, $hold, @pairs);

    if($ENV{'REQUEST_METHOD'} eq 'GET') {
        @pairs = split(/&/, $ENV{'QUERY_STRING'});
    } else {
        read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
        @pairs = split(/&/, $buffer);
    }
    foreach $pair (@pairs) {
	($name, $value) = split(/=/, $pair);
	$value =~ tr/+/ /;
	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
	$name =~ s/\n//g;
	$name =~ s/\r//g;
	$value =~ s/\n//g;
	$value =~ s/\r//g;

      #This section checks the value portion (user input) of
      #all name/value pairs.
	$value =~ s/<.+>?//g;		#Remove this tag to permit HTML tags
	$value =~ s/\|//g;
	$value =~ s/<!--(.|\n)*-->//g;
	$value =~ s/\s-\w.+//g;
	$value =~ s/system\(.+//g;
	$value =~ s/grep//g;
	$value =~ s/\srm\s//g;
	$value =~ s/\srf\s//g;
	$value =~ s/\.\.([\/\:]|$)//g;
	$value =~ s/< *((SCRIPT)|(APPLET)|(EMBED))[^>]+>//ig;

      #This section checks the value portion (from element name) of
      #all name/value pairs. This was included to prevent any nasty
      #surprises from those who would hijack you forms!
        $name =~ s/<.+>?//g;
	$name =~ s/<!--(.|\n)*-->//g;
	$name =~ s/^\s-\w.+//g;
	$name =~ s/system\(.+//g;
	$name =~ s/grep//g;
	$name =~ s/\srm\s//g;
	$name =~ s/\srf\s//g;
	$name =~ s/\.\.([\/\:]|$)//g;
	$name =~ s/< *((SCRIPT)|(APPLET)|(EMBED))[^>]+>//ig;

	$FORM{$name} = $value;
    }
}


