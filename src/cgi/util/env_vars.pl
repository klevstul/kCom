#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

#####################################################################
#
#	env_vars.pl V1.0
#	Copyright 1999, Jim Melanson
#	webmaster@thebeaches.to, www.thebeaches.to/jmwd/charity_ware
#
#####################################################################
#
#	Load this script into ANY cgi-bin directory and CHMOD it to 755.
#	Then call the script through your browser by typing the full
#	URL to it in the address line. The script will print out all the
#	environment variables for the server it is loaded on.
#
#	Whenever you are putting a script on a new server, it is a good
#	ideal to run this script once so you can see what Environment
#	Variables are being returned by the server as not all servers
#	return the same environemnt variables.
#
#####################################################################


$font = "<FONT SIZE=\"-1\" FACE=\"verdana, arial, helvetica\">";
print "Content-type: text/html\n\n";

unless($ENV{'DOCUMENT_URI'}) {
    print "<HEAD><TITLE>Environment Variable Report - Copyright Jim Melanson</TITLE></HEAD>\n";
    print "<BODY TEXT=\"BLACK\" LINK=\"#8B0000\" ALINK=\"#FF0000\" VLINK=\"#B22222\" BGCOLOR=\"#FFF6FO\">\n";
}
print "<FONT SIZE=\"-1\" FACE=\"verdana, arial, helvetica\"><P>\n";
print "<CENTER><B><FONT SIZE=\"+2\">Environment Variables</FONT></B><BR>\n";
print "As returned for the server hosting<BR><BR>$ENV{'SERVER_NAME'}</B><HR WIDTH=75%>\n";
print "<TABLE BORDER=2 CELLSPACING=15>\n";
print "<TR><TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font<B>Variable Name</B></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font<B>Variable Reference</B></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font<B>Variable Value</B></TD></TR>\n";

@keys = sort(keys(%ENV));
foreach $key (@keys) {
    print "<TR><TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font $key</TD>\n";
    print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font \$ENV{'$key'}</TD>\n";
    print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font $ENV{$key}</TD></TR>\n";
}

print "</TABLE></CENTER><HR><P>\n";

unless($ENV{'DOCUMENT_URI'}) {
    print "<CENTER><B><H2>SSI Environment Variables</H2></B></CENTER>\n";
    print "<BLOCKQUOTE>There are five environement variables not shown. This\n";
    print "is because they only apply to pages calling a Server Side Include. If you\n";
    print "set this script to be called via SSI then you will see these additional\n";
    print "five variables:<P>\n";
    print "<OL><LI><B>\$ENV{'DOCUMENT_NAME'}</B> is the name of the page calling the script. (i.e. page.shtml)<P>\n";
    print "<LI><B>\$ENV{'DOCUMENT_URI'}</B> is the relative path to the page from the Root directory.<P>\n";
    print "<LI><B>\$ENV{'LAST MODIFIED'}</B> is the date/time the page calling the script was last modified.<BR>(i.e. 
Saturday, 04-Oct-97 16:23:32 PDT)<P>\n";
    print "<LI><B>\$ENV{'DATE_LOCAL'}</B> the date and time local to the server being accessed. Same format as above.<P>\n";
    print "<LI><B>\$ENV{'DATE_GMT'}</B> is the current date and time according to Greenwich Mean Time. Same Format as 
above.\n";
    print "</OL><P><HR WIDTH=75%>\n";
}

print "<CENTER><B><H2>Perl &amp; Sendmail</H2></B></CENTER>\n";
print "<BLOCKQUOTE>Two things most programs need is the location of\n";
print "Perl (this is placed on the first line of every program) and the\n";
print "location of \"sendmail\" if you program has any mail functions.<P>\n";
print "Below is the possible values for each of these. If your not sure which\n";
print "one is the right one to use, your most likely choices are emphasized, if none are\n";
print "emphasized then try them one at a time of check with your support person/system administrator.</BLOCKQUOTE><P>\n";
print "<CENTER><TABLE BORDER=0><TR><TD>$font\n";

print "<B>Location of Perl</B><BR><OL>\n";
$whereis_perl = `whereis perl`;
@perl_locations = split(" ", $whereis_perl);
@perl_locations = sort(@perl_locations);
for($pa = 0; $pa <= $#perl_locations; $pa++) {
    if(($perl_locations[$pa] eq '/usr/bin/perl') || ($perl_locations[$pa] eq '/usr/local/bin/perl')) {
        print "<LI><U><I>$perl_locations[$pa]</I></U>\n";
    } else {
        print "<LI>$perl_locations[$pa]\n";
    }
}

print "</OL><P><B>Location of Sendmail</B><BR><OL>\n";
$whereis_sendmail =`whereis sendmail`;
@sendmail_locations = split(" ",$whereis_sendmail);
@sendmail_locations = sort(@sendmail_locations);
for($sa = 0; $sa <= $#sendmail_locations; $sa++) {
    if(($sendmail_locations[$sa] eq '/usr/sbin/sendmail') || ($sendmail_locations[$sa] eq '/usr/bin/sendmail')) {
        print "<LI><U><I>$sendmail_locations[$sa]</I></U>\n";
    } else {
        print "<LI>$sendmail_locations[$sa]\n";
    }
}
print "</OL></FONT></TD></TR></TABLE><HR><P>\n";


print "<CENTER><B><H2>Environment Variable Concantenations</H2></B></CENTER>\n";
print "<BLOCKQUOTE>Here are a few Environment Variables used together or\n";
print "or in part to reference different elements of a script. Using these\n";
print "will easily allow you to manipulate your scripts on the fly where\n";
print "hard coding simply doesn't do what you want.</BLOCKQUOTE><P>\n";
print "<DL>\n";
print "<DT><B>\$script_url = \"http://\$ENV{'SERVER_NAME'}\$ENV{'SCRIPT_NAME'}\";</B>\n";
print "<DD>Using this will allow you to directly reference your script\n";
print "without having to write the full URL to the script. Therefore, if I\n";
print "now use the variable \$script_url, this is what it returns:<P>\n";
$script_url = "http://$ENV{'SERVER_NAME'}$ENV{'SCRIPT_NAME'}";
print "<CENTER><BIG><TT>$script_url</TT></BIG></CENTER><BR><HR><BR>\n";

print "<DT><B>\$data = \"\$ENV{'DOCUMENT_ROOT'}/path/to/data_directory\";</B>\n";
print "<DD>Using this will allow you to create/access a data directory\n";
print "for your program without having to specifically declare the actual\n";
print "location of the data directory for the program. Beginning October 1st, 1999\n";
print "all my programs are using this procedure to create their own data directories\n";
print "and access them. Now, whenever the program references the variable\n";
print "\$data, here is what it's actual value is:<P>\n";
$data = "$ENV{'DOCUMENT_ROOT'}/path/to/data_directory";
print "<CENTER><BIG><TT>$data</TT></BIG></CENTER><BR><HR><BR>\n";

print "<DT><B>\$script_path = \"\$ENV{'DOCUMENT_ROOT'}\$ENV{'SCRIPT_NAME'}\";</B>\n";
print "<DD>Using this will allow you to directly reference your script\n";
print "via it's absolute path. When I create a separate setup script for\n";
print "a program, I always like to have it delete itself once it has\n";
print "successfully run. Therefore, at the end of the routine I will delete\n";
print "it using the above reference, like this:<P>\n";
$script_path = "$ENV{'DOCUMENT_ROOT'}$ENV{'SCRIPT_NAME'}";
print "<CENTER><BIG><TT>unlink(\$script_path);</TT></BIG><P>\n";
print "which is actually referencing:<P>\n";
print "<BIG><TT>unlink(\'$script_path\');</TT></BIG></CENTER><BR><HR><BR>\n";
print "</DL><P>\n";

print "<CENTER><B><H2>Special Perl Variables</H2></B></CENTER>\n";
print "<BLOCKQUOTE>Here are some special Perl variables you may or\n";
print "may not be able to use in your programs.</BLOCKQUOTE><P>\n";
print "<CENTER><TABLE BORDER=2 CELLSPACING=15>\n";
print "<TR><TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font<B>Variable</B></FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font<B>Meaning</B></FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font<B>Value</B></FONT></TD></TR>\n";

print "<TR><TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font \$\]</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font Current Version of Perl on this server</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font $]</FONT></TD></TR>\n";

print "<TR><TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font \$\^\O</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font Current Server Operating System</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font $^O</FONT></TD></TR>\n";

print "<TR><TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font \$0</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font Current Program Name</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font $0</FONT></TD></TR>\n";

print "<TR><TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font \$\$</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font Current Process ID</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font $$</FONT></TD></TR>\n";

print "<TR><TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font \$\^T</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font Basetime that the script\n";
print "began running, in seconds, since the beginning of the epoch. The\n";
print "epoch began at one second after midnight on January 1st, 1970.<P>\n";
print "All returns for the file test operators -C, -A and -M are based on this value.</FONT></TD>\n";
print "<TD VALIGN=\"TOP\" ALIGN=\"CENTER\">$font $^T</FONT></TD></TR>\n";

print "</TABLE></CENTER><P>\n";

#################################################
#						#
#	      Perl Module Lister		#
#						#
#################################################

print "<CENTER><TABLE BORDER=2><TR><TD ALIGN=\"CENTER\">$font<B>Installed Perl Modules</B></FONT></TD></TR>\n";
print "<TR><TD ALIGN=\"CENTER\" VALIGN=\"TOP\">\n";
print "<TABLE BORDER=0><TR><TD VALIGN=\"TOP\">$font\n";
find(\&getmodules,@INC);
$modcount = 0;
foreach $line(@foundmods) {
    $match = lc($line);
    if ($found{$line}[0] >0) {
	$found{$line} = [$found{$line}[0]+1,$match];
    } else {
	$found{$line} = ["1",$match];
	$modcount++;
    }
}
@foundmods = sort count keys(%found);
sub count {
    return $found{$a}[1] cmp $found{$b}[1];
}
$third = $modcount/3;
    $count=0;
    foreach $mod(@foundmods){
	$count++;
	if ($count <= $third){
	    print qq~$mod<BR>~;
	} else {
	    #push (@mod1,$mod);
	    $count = 0;
	    print "</TD><TD>$font\n";
	}
    }

print "</TD></TR></TABLE></TD></TR></TABLE><P>\n";

#################################################
#						#
#	      Subs for Module Lister		#
#						#
#################################################

 
sub getmodules {
	use File::Find;
 	$count = 0;	
	if ($File::Find::name =~ /\.pm$/){
	    open(MODFILE,$File::Find::name) || return;
	    while(<MODFILE>){
		if (/^ *package +(\S+);/){
		    push (@foundmods, $1);
  		    last;
		}
   	    }
	    chomp(@foundmods);
 	}
}


print "<CENTER><HR WIDTH=55%><BR>\n";
print "env_vars.pl &copy;1999 by <A HREF=\"mailto:webmaster\@thebeaches.to\">Jim Melanson</A>.  All rights reserved.\n";
print "<P></BODY></HTML>\n";
exit;
