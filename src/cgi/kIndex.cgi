#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl


# ---------------------------------------------
# filename:	kIndex.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	23.10.2002
# version:	v01_20030211
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){							# $^O = Current Server Operating System; linux | MSWin32
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;

# ------------------
# declarations
# ------------------


# ------------------
# main
# ------------------
&kCom_printTop("kIndex");
&printMain;
&kCom_printBottom;


# ------------------
# sub
# ------------------


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
# desc:		prints out layout and calls plugins on index page
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
sub printMain{
	my @layout;
	my $layout;
	my @modules;
	my $module;

	my $mod;
	my $param;
	my $html;

	if (&kCom_login){
		@layout = &kCom_getIniValue('kIndex','LAYOUT');
	} else {
		@layout = &kCom_getIniValue('kIndex','LAYOUT_NOTLOGGEDIN');	
	}

	# before split -
	# 	orginal line: 	"<tr><td>[m1(1)]</td><td>[m2(5)]</td><td>[m3(1)]</td></tr>"
	# after split - 
	# 	$modules[0]:		"<tr><td>"
	# 	$modules[1]: 		"m1(1)]</td><td>"
	# 	$modules[2]: 		"m2(5)]</td><td>"
	# 	$modules[3]: 		"m3(1)]</td></tr>"

	foreach $layout (@layout){
		
		@modules = split(/\[/, $layout);
		
		foreach $module (@modules) {
			$mod = undef;
			$param = undef;
			if ($module =~ m/^m(\d+)\((\d+)\)\](.*)/){
				$mod = $1;
				$param = $2;
				$html = $3;
			} elsif ($module =~ m/^m(\d+)\](.*)/){
				$mod = $1;			
				$html = $2;
			} else {
				$html = $module;
			}
			if ($mod != undef){
				if ($mod == 1){
					&kCom_kAlbum_getLatest(1,$param);
				} elsif ($mod == 2){
					&kCom_kAlbum_getLatest(2,$param);
				} elsif ($mod == 3){
					&kCom_kGetMostPop("pic",$param);
				} elsif ($mod == 4){
					&kCom_kGetMostPop("album",$param);
				} elsif ($mod == 10){
					&kCom_kForum_getLatest($param);
				} elsif ($mod == 30){
					&kCom_kIndexPlugin_logViewer($param);
				} elsif ($mod == 31){
					&kCom_kIndexPlugin_contactInfo;
				} elsif ($mod == 32){
					&kCom_kIndexPlugin_kSMS;
				} elsif ($mod == 33){
					&kCom_kIndexPlugin_news($param);
				} elsif ($mod == 40){
					&kCom_kGetMostPop("movie",$param);
				}				
			}
			print "$html\n";
		}
	}
}
