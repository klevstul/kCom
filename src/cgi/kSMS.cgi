#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

# ---------------------------------------------
# filename:	kSMS.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	13.10.2003
# version:	v01_20031013
# ---------------------------------------------

# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;
use LWP::Simple;

# ------------------
# declarations
# ------------------
my %FORM = &kCom_parse;
my $phonenumber = $FORM{'phonenumber'};
my $message = $FORM{'message'};


# ------------------
# main
# ------------------
if ($phonenumber ne undef && $message ne undef){
	&kSMS_sendSMS;
}



# ------------------
# sub
# ------------------
sub kSMS_sendSMS{

	my @url 		= &kCom_getIniValue("kIndexPlugin","KSMS_URL");
	my @url2 		= &kCom_getIniValue("kIndexPlugin","KSMS_URL2");
	my @phfield		= &kCom_getIniValue("kIndexPlugin","KSMS_PHFIELD");
	my @txtfield	= &kCom_getIniValue("kIndexPlugin","KSMS_TXTFIELD");
	my @extratxt	= &kCom_getIniValue("kIndexPlugin","KSMS_EXTRATXT");

	my $result;

	my $submit_url = $url[0] . "?" . "&" . $phfield[0] . "=" . $phonenumber . "&" . $txtfield[0] . "=" . $message . " " . $extratxt[0];

	&kCom_printTop();

	print qq(Sending SMS. );

	# Note: The following lines are spesific for "email2sms.ru"!
	
	# receives a special parameters to add in the submission form
	$result = &get($url2[0]);

	# parse the result string on the format:
	# document.write('<input type=hidden name=webmaster value="send-free-sms.netfirms.com"><input type=hidden name=time value="1066022768"><input type=hidden name=sig value="a68215f79828998f841c6a03c3a176f9">');
	if ($result =~ m/.*name="?(\w+)\s\value="(.*)".*name="?(\w+)\s\value="(.*)".*name="?(\w+)\s\value="(.*)".*$/){
		$result = $1 . "=" . $2 . "&" . $3 . "=" . $4 . "&" . $5 . "=" . $6;
	}

	# rebuilds the url with the new values
	$submit_url .= "&" . $result;

	# resubmits the query
	$result = &get($submit_url);

	#print $result;

	# logs the entry
	&kCom_log("kSMS", $phonenumber.": ".$message);

	# jumps back to frontpage
	&kCom_jumpTo("kIndex.cgi",0,2);
	&kCom_printBottom;
}

