#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

# ---------------------------------------------
# filename:	kUpload.pl
# author:	Frode Klevstul (frode@klevstul.com)
# started:	20.02.2003
# version:	v01_20030330
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;
use Image::Size;


# ------------------
# declarations
# ------------------
my $album;
my @picture_dir = &kCom_getIniValue("kAlbum","PATH_ALBUM");
my @upload_dir = &kCom_getIniValue("kAlbum","PATH_UPLOAD");
my @legal_formats = (
	"jpg",		# Joint Photographic Experts Group JFIF format
	"jpeg",		# Joint Photographic Experts Group JFIF format
);


# ------------------
# main
# ------------------
&kUpload_mainMenu;


# ------------------
# sub
# ------------------
sub kUpload_mainMenu {
	print "\n\n" . "    ----------- \n";
	print          "      kUpload   \n";
	print          "    ----------- \n\n";
	
	# checks which filetypes that exists in upload directory...
	&kUpload_checkFiles;
	
	# if $album is assigned, then upload pictures
	if ($album ne "") {&kUpload_importPictures};
}


sub kUpload_checkFiles{
	my @files;
	my $file;
	my $check;
	my %formats;
	my $format;
	my $key;
	my $input;

	# checks if a file "Thumbs.db" exists (Windows system file that can be deleted)
	# if it existst unlink/delete it
	if (-e "$upload_dir[0]/Thumbs.db"){
		unlink "$upload_dir[0]/Thumbs.db";
	}


	# read all files in the upload directory
	opendir (D, "$upload_dir[0]");
	@files = grep /\w/, readdir(D);
	closedir(D);

	# goes through all files in the upload directory
	# for each file, goes through all legal formats
	# checks if the file in upload dir is a legal format
	# counts how many files there is of each filetype
	foreach $file (@files){
		foreach $format (@legal_formats){
			if ($file =~ m/\.$format$/i){
				$formats{$format}++;
				$check = "OK";
			}
		}
		# if we have a unsupported filetype, we quit
		if ($check ne "OK" ){
			&kCom_error("kUpload_checkFiles","Unsupported filetype ('$file') in upload directory");
		} else {
			# clears the check for "next round" in the foreach sentence
			$check = "";
		}
	}

	# writes out a status of filetypes in upload directory
	print "Filetypes in upload directory:\n";
	foreach $key (keys %formats){
		print "# of " . $key . "'s:\t" . $formats{$key} . "\n";
	}

	# write out import options for the user
	# jumps out of the loop when user types 'y', to import other filetypes start all over again
	FOREACH: foreach $key (keys %formats){
		if ($key eq $legal_formats[0] | $key eq $legal_formats[1]){
			print "\nType 'y' to import $key\'s into album, press 'enter' to continue to next filetype, or 'q' to quit: ";
			chop($input=<STDIN>);
			if ($input eq "y"){
				$album = &kUpload_chooseAlbum;
				last FOREACH;
			} elsif ($input eq "q"){
				exit;
			}
		}
	}
}


sub kUpload_chooseAlbum{
	my $dir;
	my @dirs;
	my $dir_counter = 1;
	my $input;
	my $date = &kCom_timestamp(1);
	my $new_album;
	my @album_status = &kCom_getIniValue("kAlbum","DEFAULT_ALBUM_STATUS");

	# --- tmp ---
	# different directory commands
	# chdir "newdir";
	# rmdir "tmp"

	# opens the album directory and finds all sub dirs	
	opendir (D, "$picture_dir[0]");
	@dirs = grep /\w/, readdir(D);
	closedir(D);

	print "Choose album by typing it's number:\n";
	foreach $dir (@dirs){
		print "  " . $dir_counter . "> " . $dir . "\n";
		$dir_counter++;
	}
	print "or type 'n' to create a new album\n";
	print "or type 'q' to quit\n";	
	print "> ";
	chop($input=<STDIN>);

	# process the input from the user
	# q - quit
	if ($input eq "q"){
		exit;
	# n - new album
	} elsif ($input eq "n"){
		print "Date of new album, format: 'YYYYMMDD' (press enter for '$date') :";
		chop($input=<STDIN>);
		if ($input ne "") {
			$date = $input;
		}
		if ($date !~ m/\d{8}/){
			&kCom_error("kUpload_chooseAlbum","Wrong date format, should be 'YYYYMMDD', not '$date'");
		}
		print "Name of new album: $date\_";
		chop($input=<STDIN>);
		$input =~ s/\s/\_/g;
		$new_album = $date . "_" . $input;
		if ($input =~ m/\-/){
			&kCom_error("kUpload_chooseAlbum","Album name can't contain the character '-' like '$input' does.");
		}
		mkdir "$picture_dir[0]/$new_album";

		# creates text file w. title etc. for the new album
		open (FILE, ">$picture_dir[0]/$new_album/$new_album\.txt");
		$input =~ s/\_/ /g;
		print "The title of this new album is: $input\n";
		print "The SUBTITLE of this new album: ";
		chop($input=<STDIN>);
		print FILE "SUBTITLE\t$input\n";
		print "WHEN are these pictures from: ";
		chop($input=<STDIN>);
		print FILE "WHEN\t$input\n";
		print "WHERE are these pictures from: ";
		chop($input=<STDIN>);
		print FILE "WHERE\t$input\n";
		print FILE "TIMESTAMP\t$date\n";
		print FILE "STATUS\t$album_status[0]\n";
		close (FILE);

		&kCom_log("kUpload","newAlbum:$new_album");

		return "$new_album";
	# choose existing album
	} elsif ($input > 0 and $input < $dir_counter){
		return $dirs[($input-1)];
	# unknown option
	} else {
		&kCom_error("kUpload_chooseAlbum","A album with that number doesn't exist");
	}
}


sub kUpload_importPictures{
	my $input;
	my @files;
	my $file;
	my $new_pic_name;
	my $new_pic_name_tmp;
	my $counter = 0;
	my @thumb_width = &kCom_getIniValue("kAlbum","THUMB_WIDTH");
	my @thumb_height = &kCom_getIniValue("kAlbum","THUMB_HEIGHT");
	my $cmd_line;
	my @nconvert = &kCom_getIniValue("kAlbum","NCONVERT");
	my $width;
	my $height;
	my $line;
	my @max_width = &kCom_getIniValue("kAlbum","MAX_WIDTH");
	my @max_height = &kCom_getIniValue("kAlbum","MAX_HEIGHT");
	my @pic_quality = &kCom_getIniValue("kAlbum","PIC_QUALITY");
	my @thumb_quality = &kCom_getIniValue("kAlbum","THUMB_QUALITY");
	my $date;
	my @preview_pic_win = &kCom_getIniValue("kAlbum","PREVIEW_PIC_XP");
	my @picture_status = &kCom_getIniValue("kAlbum","DEFAULT_PIC_STATUS");
	my $noPic = 0;


	print "Importing all picturefiles into the album '$album'\n";
	print "Type 'c' to continue, or anything else to quit: ";
	chop($input=<STDIN>);
	if ($input ne "c"){
		exit;
	}

	opendir (D, "$upload_dir[0]");
	@files = grep /\w/, readdir(D);
	closedir(D);

	foreach $file (@files){
		if ($file =~ m/($legal_formats[0])|($legal_formats[1])/i){
			$new_pic_name = $album;
			$new_pic_name .= "-" . $file;
			$new_pic_name =~ s/\..*$/\.jpg/;						# change 'jpeg', 'JPG' etc to 'jpg'
			$new_pic_name =~ s/\s/_/;								# swap/substitute '\s' with '_'
			$counter = 0;											# resets the counter

			while (-e "$picture_dir[0]/$album/$new_pic_name"){		# This loop prevents that filenames with same name is overwritten. 
				$new_pic_name =~ s/\.jpg//sg;						# removes the filetype

				if ($new_pic_name =~ m/--(\d+)$/){					# if the filename ends with '--' and a number
					$counter = $1;
					$counter++;
					$new_pic_name =~ s/--(\d+)$/--$counter\.jpg/;	# we increase the number with one in the new filname
				} else {
					$counter++;
					$new_pic_name .= "--" . $counter . ".jpg";
				}				
			}

			
			$new_pic_name_tmp = $new_pic_name;						# assigns the tmp name to new name without filetype ending
			$new_pic_name_tmp =~ s/\.jpg//sg;

			print "Converts '$file' into '$new_pic_name' and generates thumbnail...\n";

			# Creates thumbnails out of the picture
			if ($^O eq "MSWin32"){
				$cmd_line = "$nconvert[0] -quiet -q $thumb_quality[0] -out jpeg -resize $thumb_width[0] $thumb_height[0] -o $upload_dir[0]\\$new_pic_name_tmp\_thumb.jpg $upload_dir[0]\\$file";
				$cmd_line =~ s/\\/\//g;
			} elsif ($^O eq "linux"){
				$cmd_line = "djpeg $upload_dir[0]/$file | pnmscale -xsize $thumb_width[0] | cjpeg -quality $thumb_quality[0] > $upload_dir[0]/$new_pic_name_tmp\_thumb.jpg";
			}
			system("$cmd_line");
			
			# Processing original pictures
			if ($^O eq "MSWin32"){
				$cmd_line = "$nconvert[0] -info $upload_dir[0]\\$file >pic.tmp";
				system("$cmd_line");												# gets picture information
				$cmd_line = "";														# resets the command line

				open(FILE, "<pic.tmp") || &kCom_error("kUpload_importPictures","failed to open 'pic.tmp'");
				while ($line = <FILE>){
					if ($line =~ m/^\s+Width\s+:\s(\d+)/){
						$width = $1;
					} elsif ($line =~ m/^\s+Height\s+:\s(\d+)/){
						$height = $1;
					}
				}
				close (FILE);

				if ( ($height > $max_height[0]) & ($width > $max_width[0]) ){
					if ($height > $width){
						$cmd_line = "$nconvert[0] -quiet -q $pic_quality[0] -out jpeg -ratio -resize 0 $max_height[0] $upload_dir[0]\\$file";
					} else {
						$cmd_line = "$nconvert[0] -quiet -q $pic_quality[0] -out jpeg -ratio -resize $max_width[0] 0 $upload_dir[0]\\$file";						
					}
				} elsif ($height > $max_height[0]){
					$cmd_line = "$nconvert[0] -quiet -q $pic_quality[0] -out jpeg -ratio -resize 0 $max_height[0] $upload_dir[0]\\$file";
				} elsif ($width > $max_width[0]){
					$cmd_line = "$nconvert[0] -quiet -q $pic_quality[0] -out jpeg -ratio -resize $max_width[0] 0 $upload_dir[0]\\$file";						
				}

			} elsif ($^O eq "linux"){

				($width, $height) = &imgsize("$upload_dir[0]/$file");

				if ( ($height > $max_height[0]) & ($width > $max_width[0]) ){
					if ($height > $width){
						$cmd_line = "djpeg $upload_dir[0]/$file | pnmscale -ysize $max_height[0] | cjpeg -quality $pic_quality[0] > $upload_dir[0]/$file";
					} else {
						$cmd_line = "djpeg $upload_dir[0]/$file | pnmscale -xsize $max_width[0] | cjpeg -quality $pic_quality[0] > $upload_dir[0]/$file";
					}
				} elsif ($height > $max_height[0]){
					$cmd_line = "djpeg $upload_dir[0]/$file | pnmscale -ysize $max_height[0] | cjpeg -quality $pic_quality[0] > $upload_dir[0]/$file";
				} elsif ($width > $max_width[0]){
					$cmd_line = "djpeg $upload_dir[0]/$file | pnmscale -xsize $max_width[0] | cjpeg -quality $pic_quality[0] > $upload_dir[0]/$file";
				}			
			}

			if ($cmd_line ne ""){
				system("$cmd_line");											# changes the picture into right size
			}


			rename "$upload_dir[0]/$file" , "$upload_dir[0]/$new_pic_name";		# change name on picture


			# creates text file w. picture text
			open (FILE, ">$upload_dir[0]/$new_pic_name_tmp\.txt");
			print "Add text to picture '$file' ('$new_pic_name'): \n";

			if ($^O eq "MSWin32"){
				print "Type 'p' to see a preview of the picture, anything else to continue: ";
				chop($input=<STDIN>);
				if ($input eq "p"){
					print "Close the picture (XnView) to continue...\n";
					system ("$preview_pic_win[0]" . "$new_pic_name");
				}
			}

			print "Description of picture: ";
			chop($input=<STDIN>);
			print FILE "DESCRIPTION\t$input\n";
			$date = &kCom_timestamp();
			print FILE "TIMESTAMP\t$date\n";
			print FILE "STATUS\t$picture_status[0]\n";
			close (FILE);


			# moves pictures text file into it's album
			rename "$upload_dir[0]/$new_pic_name_tmp\.txt" , "$picture_dir[0]/$album/$new_pic_name_tmp\.txt";
			# moves picture into it's album
			rename "$upload_dir[0]/$new_pic_name" , "$picture_dir[0]/$album/$new_pic_name";
			# moves thumbnail into it's album
			rename "$upload_dir[0]/$new_pic_name_tmp\_thumb.jpg" , "$picture_dir[0]/$album/$new_pic_name_tmp\_thumb.jpg";

			$noPic++;

		}
	}

			&kCom_log("kUpload","newPics:$noPic:$album");

	print "The pictures are published.\n";
}
