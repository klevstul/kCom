#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kImgFromText.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	20.12.2006
# version:	v01_20061220
$VERSION = 'v01_20061220';
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;
use GD;

# ------------------
# declarations
# ------------------
my %FORM			= &kCom_parse;
my $string			= $FORM{'string'};
my $isCode			= $FORM{'isCode'};


# ------------------
# main
# ------------------
if ($string ne ""){
	&kImgFromText_generateImage($string, $isCode);
}


# ------------------
# sub
# ------------------
sub kImgFromText_generateImage {
	my $string = $_[0];
	my $isCode = $_[1];

	if ($isCode ne undef){
		$string = $string / 180876;
	}

	# We MUST do this on Windows or the
	# image will be garbled, and it
	# doesn't hurt on Unix/Linux/etc
	binmode STDOUT;

	# Output the right content type
	# for a PNG-format image
 	print "Content-type: image/png\n\n";

 	# Draw an image with a red rectangle
 	# in the middle
 	my $image = new GD::Image(30, 13);
 	my $black = $image->colorAllocate(0, 0, 0);
 	my $white = $image->colorAllocate(255, 255, 255);
 	#$image->filledRectangle(25, 25, 75, 75, $red);
 	$image->string(gdSmallFont,5,1,$string,$white);
 	# Output the image to the browser
 	print $image->png;

}
