#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kLogin.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	24.10.2004
# version:	v01_20041024
# ---------------------------------------------



# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;



# ------------------
# declarations
# ------------------
my %FORM			= &kCom_parse;
my $password_form	= $FORM{'password'};
my $url				= $FORM{'url'};
my $action			= $FORM{'action'};

my @password		= &kCom_getIniValue("kLogin","PASSWORD");
my @domain			= &kCom_getIniValue("kLogin","DOMAIN");

my $exp_date = 0;
my $domain = $domain[0];
my $path = "/";

# ------------------
# main
# ------------------
if ($action eq "logout") {
	&kLogin_logout;
} else {
	&kLogin_login;
}




# ------------------
# sub
# ------------------
sub kLogin_login{

	#my $date = time;

	if ($password_form eq $password[0]){
		&kCom_setCookie("kCom", "loggedIn",  $exp_date, $path, $domain);	
	}

	if ($password_form eq $password[0]){
		&kCom_printTop;
		print("<br><br>Logging in...<br>");
		&kCom_jumpTo($url, 0, 3);
		&kCom_printBottom;
	} else {
		&kCom_printTop;
		&kCom_printError("kLogin","Wrong password, access denied.");
		&kCom_jumpTo($url, 0, 3);
		&kCom_printBottom;
	}
}

sub kLogin_logout{

	my $date = time;

	&kCom_setCookie("kCom", "loggedIn",  0.001, $path, $domain);	

	&kCom_printTop;
	print("<br><br>Logging out...<br>");
	&kCom_jumpTo("kIndex.cgi", 0, 3);
	&kCom_printBottom;
}
