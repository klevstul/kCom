#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/local/bin/perl

# ---------------------------------------------
# filename:	kNews.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	01.11.2003
# version:	v01_20031101
# ---------------------------------------------

# ----------------------------------
# use
# ----------------------------------
use strict;
if ($^O eq "linux"){
	use lib '/home/klevstul/www/kCom/cgi/';
}
use kCom;

# ------------------
# declarations
# ------------------
my %FORM = &kCom_parse;
my $message = $FORM{'message'};
my $link = $FORM{'link'};
my $pwd = $FORM{'p'};
my $password = "kCom";


# ------------------
# main
# ------------------
&kCom_printTop;
if ($message ne undef && $pwd eq $password){
	&kNews_add;
} elsif ($pwd eq $password) {
	&kNews_form;
}
&kCom_printBottom;



# ------------------
# sub
# ------------------
sub kNews_add{

	my @newsfile 	= &kCom_getIniValue("kNews","PATH_NEWSFILE");
	my $timestamp	= &kCom_timestamp();
	my $host_ip		= &kCom_getHostIP;

	if ($link ne undef){
		$message = "<a href=\"$link\">$message</a>";
	}

	open (FILE, "<$newsfile[0]") || &kCom_error("kNews_add","failed to open '$newsfile[0]'");
	flock (FILE, 1);		# share reading, don't allow writing
	my $content = join("",(<FILE>));
	close (FILE);
	flock (FILE, 8);		# unlock file

	open (FILE, ">$newsfile[0]") || &kCom_error("kNews_add","failed to open '$newsfile[0]'");
	flock (FILE, 2);		# lock file for writing
	print FILE "$timestamp���$host_ip���$message\n" . $content;
	close (FILE);
	flock (FILE, 8);		# unlock file


	# jumps back to frontpage
	&kCom_jumpTo("kIndex.cgi",0,2);
}


sub kNews_form{

	&kCom_tableStart("Add news","95%");
	print qq(
		<form method="POST" name="form" action="kNews.cgi">
		<input type="hidden" name="p" value="$pwd">
		<table border="0" callpadding="0" cellspacing="0">
		<tr>
			<td>message:</td>
			<td><input type="text" name="message" size="40" maxlength="35">
		</tr>
		<tr>
			<td>link:</td>
			<td><input type="text" name="link" size="50" maxlength="150">
		</tr>
		<tr>
			<td colspan="2" align="right"><a href="javascript:document.form.submit()">add news</a></td>
		</tr>
		</table>
	);
	&kCom_tableStop;

}