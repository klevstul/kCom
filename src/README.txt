# --------------------------------------------- #
#          kCom - Interactive Homepage          #
#												#
#               version: v0.1                   #
# - - - - - - - - - - - - - - - - - - - - - - - #
#          100% perl - keep it simple           #
# - - - - - - - - - - - - - - - - - - - - - - - #
#          Developed by Frode Klevstul          #
#             (frode@klevstul.com)              #
# --------------------------------------------- #

LOGFORMAT:
LogFormat "%h %l %u %t \"%r\" %>s %b" common 


# ---------------------------------------------
# Available modules
# ---------------------------------------------
kIndex	This is the core module that starts the entire
		site. This module is called directly from 
		"kCom/index.cgi". kIndex chooses which
		modules that will be started, and the layout on the page.

kMovies	This is a multimedia module for showning 
		your own videos, vote for videos, write comments on videos,
		send videos to friends. All videoes are played in the browser
		with an activeX video component from Microsoft.


# kAlbum
# kFora
# kOther
# kDownload
# kViewpage
# kLinks
# kTravelog
# kPoll

# ---------------------------------------------
# Available plugins
# ---------------------------------------------

# PLUG-INS
----------
# kNews			(plugin for kIndex)
# kContactInfo	(plugin for kIndex)


# filetypes:
.ini	- initialization files
.src	- source files
.log	- log files
.txt	- text files
.com	- comment files
.vot	- vote files
.pop	- most popular 
.new	- newest entries
.tvo	- top voted

# location of different types of files
.... kCom/.ini
.... kCom/cgi/.cgi
.... kCom/img/.gif
.... kCom/[module]/.src .ini .new .pop .tvo .log
.... kCom/[module]/com/.com
.... kCom/[module]/vot/.vot


# trenger ett system for � l�se filer, slik
# at ting ikke forsvinner dersom to personer 
# f.eks �nsker � legge inn kommentar samtidig



kLibMain_v01.pm



kLibViewpage_v01.pm
	@filetypes{
		'html	/home/klevstul/www',
		'cgi	/home/klevstul/www/source', # THIS SHOULD NOT BE THE SAME AS YOUR OWN CGI DIRECTORY
		'txt	/home/klevstul/www'
	}
	
	view_file(&get_path($filename));
		


kLibAlbum_v01.pm


kAlbum_v01.cgi
	- latest (new) entries
	- most popular
	- last visited entries


kSetup.pl
	input: www-directory, cgi-directory
	generer opp n�dvendige kataloger, tar backup av gamle filer??, legger over nye filer.



dir:
----
/home/klevstul/www/cgi/kCom/
/home/klevstul/www/kCom/

	

